package cmu.haisheny;
import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class InvertedIndex {

	public static class Map extends Mapper<LongWritable, Text, Text, Text> {

		private Text word = new Text();
	    private Text location = new Text();

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			FileSplit fileSplit = (FileSplit) context.getInputSplit();
			String fileName = fileSplit.getPath().getName();
			location.set(fileName);
			
			String line = value.toString().replaceAll("\\p{Punct}", " ").trim();
			StringTokenizer tokenizer = new StringTokenizer(line.toLowerCase());
			while (tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken().trim();
				word.set(token);
				context.write(word, location);
			}
		}
	}
	
	public static class Combiner extends Reducer<Text, Text, Text, Text> {
		protected void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			Set<Text> uniques = new HashSet<Text>();
			for (Text value : values) {
				if (uniques.add(value)) {
					context.write(key, value);
				}
			}
		}
	}

	public static class Reduce extends Reducer<Text, Text, Text, Text> {
		private Text fileNames = new Text();
		
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			StringBuilder toReturn = new StringBuilder();
			Set<Text> uniques = new HashSet<Text>();
			for (Text value : values) {
				if (uniques.add(value)) {
					toReturn.append(" ");
					toReturn.append(value.toString());
				}
			}
			fileNames.set(toReturn.toString());
			context.write(key, fileNames);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		conf.set("mapred.textoutputformat.separator", " :");
		Job job = new Job(conf, "InvertedIndex");

		job.setJarByClass(InvertedIndex.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		job.setMapperClass(Map.class);
		job.setCombinerClass(Combiner.class);
		job.setReducerClass(Reduce.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);
	}

}
