package cmu.haisheny;
import java.io.IOException;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LangModelGenerator {
	
	private static final String TABLE_NAME = "wp";
	private static final String JOB1_NAME = "LangModelGeneratorJob";
	private static final String JOB2_NAME = "LangModelWriteHFileJob";
	private static final String PHRASE_BASE = "~:";
	private static final String CONFIG_THRESHOLD = "wordpairs.threshold";
	private static final String CONFIG_TOP_WORDS = "wordpairs.topwords";
	
	public static Logger log = LoggerFactory.getLogger(LangModelGenerator.class);
	
	public static class LoadMapper extends Mapper<LongWritable, Text, Text, Text> {
		private Text wordcount = new Text();
		private Text phrase = new Text();
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			long threshold = context.getConfiguration().getInt(CONFIG_THRESHOLD, 3);
			log.info(CONFIG_THRESHOLD+" = "+threshold);
			String[] pairs = value.toString().trim().split("\t");
			String words = pairs[0].trim();
			String count = pairs[1].trim();
			if(Long.valueOf(count) <= threshold) {
				return;
			}
			phrase.set(words);
			wordcount.set(PHRASE_BASE+count);
			context.write(phrase, wordcount);
			
			int sindex = words.lastIndexOf(" ");
			if(sindex != -1) {
				String mainPhrase = words.substring(0, sindex);
				String word = words.substring(sindex+1);
				phrase.set(mainPhrase);
				wordcount.set(word+":"+count);
				context.write(phrase, wordcount);
			}
		}
	}
	
	public static class LoadReducer extends Reducer<Text, Text, Text, Text> {
		private Text pairs = new Text();
		// This is		a:0.50,an:0.25,some:0.10
		public void reduce(Text key, Iterable<Text> values, Context context)
				throws IOException, InterruptedException {
			int topN = context.getConfiguration().getInt(CONFIG_TOP_WORDS, 10);
			log.info(CONFIG_TOP_WORDS+" = "+topN);
			long baseNum = 0l;
			boolean found = false;
			StringBuilder sb = new StringBuilder();
			Set<String> filteredValue = new TreeSet<String>(new Comparator<String>() {
				@Override
				public int compare(String arg0, String arg1) {
					int count0 = Integer.valueOf(arg0.split(":")[1]);
					int count1 = Integer.valueOf(arg1.split(":")[1]);
					return count1 - count0;
				}
			});
			
			for (Text value : values) {
				if(!found && value.toString().startsWith(PHRASE_BASE)) {
					String baseValue = value.toString().substring(PHRASE_BASE.length());
					baseNum = Long.valueOf(baseValue);
					found = true;
				} else {
					filteredValue.add(value.toString());
				}
			}
			if(filteredValue.size() <= 0) {
				return;
			}
			int index = 0;
			for(String value: filteredValue) {
				if(index++ >= topN) {
					break;
				}
				String[] pair = value.split(":");
				String word = pair[0];
				long count = Long.valueOf(pair[1]);
				float probability = (float)count / (float)baseNum;
				sb.append(word).append(":").append(probability).append(",");
			}
			pairs.set(sb.toString());
			context.write(key, pairs);
		}
	}
	
	public static class HFileMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {
		private static final byte[] COL_FAMILY = Bytes.toBytes("wordpairs");
		
		@Override
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String[] pairs = value.toString().split("\t");
			String phrase = pairs[0].trim(); // i.e. This is
			String words = pairs[1].trim();	 // i.e. a:0.50,an:0.25,some:0.10
			
			byte[] rowKey = Bytes.toBytes(phrase);
            ImmutableBytesWritable rowKeyWritable = new ImmutableBytesWritable(rowKey);

            Put put = new Put(rowKey);
            for(String wordProb: words.split(",")) {
            	String[] wordProbPair = wordProb.split(":");
            	byte[] qualifier = Bytes.toBytes(wordProbPair[0]);
                byte[] cellValue = Bytes.toBytes(wordProbPair[1]);
                put.add(COL_FAMILY, qualifier, cellValue);
            }
            
            context.write(rowKeyWritable, put);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration config = new Configuration();
		String[] otherArgs = new GenericOptionsParser(config, args).getRemainingArgs();
		
		Job job = new Job(config, JOB1_NAME);
		job.setJarByClass(LangModelGenerator.class);
		job.setMapperClass(LoadMapper.class);
		job.setReducerClass(LoadReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));
		job.waitForCompletion(true);
		
		Job job2 = new Job(config, JOB2_NAME);
		job2.setJarByClass(LangModelGenerator.class);
		job2.setMapperClass(HFileMapper.class);
		job2.setMapOutputKeyClass(ImmutableBytesWritable.class);
		job2.setMapOutputValueClass(Put.class);
         
        FileInputFormat.addInputPath(job2, new Path(otherArgs[1]));
        FileOutputFormat.setOutputPath(job2, new Path(otherArgs[2]));

        Configuration hbaseConf = HBaseConfiguration.create();
        HTable wpTable = new HTable(hbaseConf, TABLE_NAME);
        HFileOutputFormat.configureIncrementalLoad(job2, wpTable);
        job2.waitForCompletion(true);
         
        LoadIncrementalHFiles loader = new LoadIncrementalHFiles(hbaseConf);
        loader.doBulkLoad(new Path(otherArgs[2]), wpTable);
	}
}
