package cmu.haisheny;
import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class NGramGenerator {

	public static class NGramMapper extends Mapper<LongWritable, Text, Text, LongWritable> {

		private Text word = new Text();
		private final static LongWritable one = new LongWritable(1);

		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			String line = value.toString().replaceAll("[\\W_0-9]", " ").trim();
			if(line.isEmpty()) {
				return;
			}
			String[] words = line.toLowerCase().split("\\s+");
			List<String> wordList = new ArrayList<String>(Arrays.asList(words));
			
			for(int i=2; i<=5; i++) {
				for(int j=0; j<=words.length-i; j++) {
					StringBuilder concatWord = new StringBuilder(words[j]);
					for(int k=1; k<i; k++) {
						concatWord.append(" ").append(words[j+k]);
					}
					wordList.add(concatWord.toString());
				}
			}
			
			for (String item : wordList) {
				word.set(item);
				context.write(word, one);
			}
		}
	}
	
	public static class NGramCombiner extends Reducer<Text, LongWritable, Text, LongWritable> {
		private LongWritable count = new LongWritable();
		
		public void reduce(Text key, Iterable<LongWritable> values, Context context)
				throws IOException, InterruptedException {
			long sum = 0;
			
			for (LongWritable value : values) {
				sum += value.get();
			}
			count.set(sum);
			context.write(key, count);
		}
	}

	public static class NGramReducer extends Reducer<Text, LongWritable, Text, LongWritable> {
		private LongWritable count = new LongWritable();
		
		public void reduce(Text key, Iterable<LongWritable> values, Context context)
				throws IOException, InterruptedException {
			long sum = 0;
			
			for (LongWritable value : values) {
				sum += value.get();
			}
			count.set(sum);
			context.write(key, count);
		}
	}

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		conf.set("mapred.textoutputformat.separator", "\t");
		Job job = new Job(conf, "NGramGenerator");

		job.setJarByClass(NGramGenerator.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(LongWritable.class);
		
		job.setMapperClass(NGramMapper.class);
		job.setCombinerClass(NGramCombiner.class);
		job.setReducerClass(NGramReducer.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.waitForCompletion(true);
	}

}
